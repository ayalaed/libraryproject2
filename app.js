var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
//var users = require('./routes/users');
var LibEmployee = require('./routes/LibEmployee_routes');
var EmpAddressPerm = require('./routes/EmpAddressPerm_routes');
var EmpAddressLocal = require('./routes/EmpAddressLocal_routes');
var EmployeeStatus = require('./routes/EmployeeStatus_routes');
var LibDepartment = require('./routes/LibDepartment_routes');
var LibHoursWage = require('./routes/LibHoursWage_routes');
var LibEvents = require('./routes/LibEvents_routes');
var Hiring = require('./routes/Hiring_routes');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/LibEmployee', LibEmployee);
app.use('/EmpAddressPerm', EmpAddressPerm);
app.use('/EmpAddressLocal', EmpAddressLocal);
app.use('/EmployeeStatus', EmployeeStatus);
app.use('/LibDepartment', LibDepartment);
app.use('/LibHoursWage', LibHoursWage);
app.use('/LibEvents', LibEvents);
app.use('/Hiring', Hiring);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
