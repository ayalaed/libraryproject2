var mysql = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace vie1q1qw company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM LibEvents;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(LibDeptNum, callback) {
    var query ='SELECT * from LibEvents where LibDeptEvents = ' +
        '(select LibDeptNum from LibDepartment) = ;';
    var queryData = [LibDeptNum];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    // for all 3 only the first two var query
    var query = 'INSERT INTO LibEvents (LibDeptNum, Location, DateOfEvent, NumOfEmpWorking) VALUES (?,?,?,?)';

    var queryData = [params.LibDeptNum, params.Location, params.DateOfEvent, params.NumOfEmpWorking];


    connection. query(query, queryData, function(err, result){
        callback(err, result);
    });


};

exports.update = function(params, callback)
{
    var query = 'UPDATE LibEvents SET LibDeptNum = ? , Location = ? ,' +
        'DateOfEvent, NumOfEmpWorking = ? WHERE LibDeptNum = ?;';
    var queryData =[params.LibDeptNum, params.Location, params.DateOfEvent, params.NumOfEmpWorking];

    connection.query(query, queryData, function(err, result)
    {
        callback(err, result);
    });
};

exports.delete = function(LibDeptNum, callback) {
    var query = 'DELETE FROM LibEvents WHERE LibEvents = ?';
    var queryData = [LibDeptNum];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var LibEventsInsert = function(LibDeptNum, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO LibDeptNum (LibDeptNum) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var LibEventsData = [];
    for(var i=0; i < LibEventsIdArray.length; i++) {
        LibEventsData.push([LibDeptNum, LibEventsIdArray[i]]);
    }
    connection.query(query, [LibEventsData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.LibEventsInsert = LibEventsInsert;


/*
 //declare the function so it can be used locally
 var skillDeleteAll = function(skill_id, callback){
 var query = 'DELETE FROM skill WHERE skill_id = ?';
 var queryData = [skill_id];

 connection.query(query, queryData, function(err, result) {
 callback(err, result);
 });
 };
 //export the same function so it can be used by external callers
 module.exports.skillDeleteAll = skillDeleteAll;
 */


exports.update = function(params, callback) {
    var query = 'UPDATE LibEvents SET LibDeptNum = ?, Location = ? ,' +
        'DateOfEvent = ?, NumOfEmpWorking WHERE LibDeptNum = ?';
    var queryData = [params.LibDeptNum, params.Location, params.DateOfEvent, params.NumOfEmpWorking];

    //   connection.query(query, queryData, function(err, result) {
    //delete company_address entries for this company
    //     skillDeleteAll(params.skill_id, function(err, result){

    //       if(params.skill_id != null) {
    //insert company_address ids
    //         skillInsert(params.skill_id, params.skill_id, function(err, result){
    //           callback(err, result);
    //     });}
    connection.query(query, queryData, function(err, result) {
        {
            callback(err, result);
        }
    });


};

/*  Stored procedure used in

 this example
 DROP PROCEDURE IF EXISTS company_getinfo;

 DELIMITER //
 CREATE PROCEDURE company_getinfo (company_id int)
 BEGIN

 SELECT * FROM company WHERE company_id = _company_id;

 SELECT a.*, s.company_id FROM address a
 LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL company_getinfo (4);

 */

exports.edit = function(LibDeptNum, callback) {
    var query = 'CALL LibEvents_getinfo(?)';
    var queryData = [params.LibDeptNum];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};