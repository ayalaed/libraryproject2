var express = require('express');
var router = express.Router();
var EmpAddressPerm = require('../model/EmpAddressPerm_dal');

// View All companys
router.get('/all', function(req, res) {
    EmpAddressPerm_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('EmpAddressPerm/EmpAddressPermViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.SSUID == null) {
        res.send('SSUID is null');
    }
    else {
        EmpAddressPerm_dal.getById(req.query.SSUID, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('EmpAddressPerm/EmpAddressPermViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form (
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    EmpAddressPerm_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('EmpAddressPerm/EmpAddressPermAdd', {'SSUID': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.SSUID == null)
    {
        res.send('A SSUID  must be provided ');
    }
    // else if(req.query.Fname == null)
    //{
    //  res.send('A first name must be provided');
    //}
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        EmpAddressPerm_dal.insert(req.query, function(err,result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/EmpAddressPerm/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.SSUID == null) {
        res.send('A SSU id is required');
    }
    else {
        EmpAddressPerm_dal.edit(req.query.SSUID, function(err, result){
            res.render('EmpAddressPerm/EmpAddressPermUpdate', {SSUID: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.SSUID == null) {
        res.send('A SSU id is required');
    }
    else {
        EmpAddressPerm_dal.getById(req.query.SSUID, function(err, SSUID){
            EmpAddressPerm_dal.getAll(function(err, SSUID) {
                res.render('EmpAddressPerm/EmpAddressPermUpdate', {SSUID: SSUID[0]});
            });
        });
    }

});


router.get('/update', function(req, res) {
    EmpAddressPerm_dal.update(req.query, function(err, result){
        res.redirect(302, '/EmpAddressPerm/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.SSUID == null) {
        res.send('SSUID is null');
    }
    else {
        EmpAddressPerm_dal.delete(req.query.SSUID, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/EmpAddressPerm/all');
            }
        });
    }
});

module.exports = router;