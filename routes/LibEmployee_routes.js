var express = require('express');
var router = express.Router();
var LibEmployee_dal = require('../model/LibEmployee_dal');


// View All companys
router.get('/all', function(req, res) {
    LibEmployee_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('LibEmployee/LibEmployeeViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.SSUID == null) {
        res.send('SSUID is null ');
    }
    else {
        LibEmployee_dal.getById(req.query.SSUID, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('LibEmployee/LibEmployeeViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form (
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    LibEmployee_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('LibEmployee/LibEmployeeAdd', {'LibEmployee': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.Fname == null)
    {
        res.send('A first name  must be provided  ');
    }
    else if(req.query.Lname == null)
    {
        res.send('A Last name must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        LibEmployee_dal.insert(req.query, function(err,result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/LibEmployee/all');


            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.SSUID == null) {
        res.send('A SSU id is required');
    }
    else {
        LibEmployee_dal.edit(req.query.SSUID, function(err, result){
            res.render('LibEmployee/LibEmployeeUpdate', {LibEmployee: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.SSUID == null) {
        res.send('A SSU id is required');
    }
    else {
        LibEmployee_dal.getById(req.query.SSUID, function(err, LibEmployee){
            LibEmployee_dal.getAll(function(err, LibEmployee) {
                res.render('LibEmployee/LibEmployeeUpdate', {LibEmployee: LibEmployee[0]});
            });
        });
    }

});


router.get('/update', function(req, res) {
    LibEmployee_dal.update(req.query, function(err, result){
        res.redirect(302, '/LibEmployee/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.SSUID == null) {
        res.send('SSUID is null');
    }
    else {
        LibEmployee_dal.delete(req.query.SSUID, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/LibEmployee/all');
            }
        });
    }
});

module.exports = router;