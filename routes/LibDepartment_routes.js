var express = require('express');
var router = express.Router();
var LibDepartment_dal = require('../model/LibDepartment_dal');


// View All companys
router.get('/all', function(req, res) {
    LibDepartment_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('LibDepartment/LibDepartmentViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.LibDeptNum == null) {
        res.send('LibDeptNum is null');
    }
    else {
        LibDepartment_dal.getById(req.query.LibDeptNum, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('LibDepartment/LibDepartmentViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form (
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    LibDepartment_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('LibDepartment/LibDepartmentAdd', {'LibDepartment': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.LibDeptNum == null)
    {
        res.send('A LibDeptNum  must be provided ');
    }
    // else if(req.query.LibDeptNum == null)
    //{
    //  res.send('A first name must be provided');
    //}
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        LibEmployee_dal.insert(req.query, function(err,result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/LibDepartment/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.LibDeptNum == null) {
        res.send('A LibDeptNum id is required');
    }
    else {
        LibDepartment_dal.edit(req.query.LibDeptNum, function(err, result){
            res.render('LibDepartment/LibDepartmentUpdate', {LibDepartment: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.LibDeptNum == null) {
        res.send('A LibDeptNum id is required');
    }
    else {
        LibDepartment_dal.getById(req.query.LibDeptNum, function(err, LibDepartment){
            LibDepartment_dal.getAll(function(err, LibDepartment) {
                res.render('LibDepartment/LibDepartmentUpdate', {LibDepartment: LibDepartment[0]});
            });
        });
    }

});


router.get('/update', function(req, res) {
    LibDepartment_dal.update(req.query, function(err, result){
        res.redirect(302, '/LibDepartment/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.LibDeptNum == null) {
        res.send('LibDeptNum is null');
    }
    else {
        LibDepartment_dal.delete(req.query.LibDeptNum, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/LibDepartment/all');
            }
        });
    }
});

module.exports = router;