var express = require('express');
var router = express.Router();
var EmployeeStatus_dal = require('../model/EmployeeStatus_dal');


// View All companys
router.get('/all', function(req, res) {
    EmployeeStatus_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('EmployeeStatus/EmployeeStatusViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.SSUID == null) {
        res.send('SSUID is null');
    }
    else {
        EmployeeStatus_dal.getById(req.query.SSUID, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('EmployeeStatus/EmployeeStatusViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form (
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    EmployeeStatus_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('EmployeeStatus/EmployeeStatusAdd', {'EmployeeStatus': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.SSUID == null)
    {
        res.send('A SSUID  must be provided ');
    }
    //  if(req.query.StudentAssistant == null)
    //{
    //  res.send('A Student Assistant must be provided');
    //}
    //else if(req.query.Standing == null)
    // {
    //   res.send('A standing must be provided');
    //}
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        EmployeeStatus_dal.insert(req.query, function(err,result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/EmployeeStatus/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.SSUID == null) {
        res.send('A SSU id is required');
    }
    else {
        EmployeeStatus_dal.edit(req.query.SSUID, function(err, result){
            res.render('EmployeeStatus/EmployeeStatusUpdate', {EmployeeStatus: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.SSUID == null) {
        res.send('A SSU id is required');
    }
    else {
        EmployeeStatus_dal.getById(req.query.SSUID, function(err, EmployeeStatus){
            EmployeeStatus_dal.getAll(function(err, EmployeeStatus) {
                res.render('EmployeeStatus/EmployeeStatusUpdate', {EmployeeStatus: EmployeeStatus[0]});
            });
        });
    }

});


router.get('/update', function(req, res) {
    EmployeeStatus_dal.update(req.query, function(err, result){
        res.redirect(302, '/EmployeeStatus/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.SSUID == null) {
        res.send('SSUID is null');
    }
    else {
        EmployeeStatus_dal.delete(req.query.SSUID, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/EmployeeStatus/all');
            }
        });
    }
});

module.exports = router;