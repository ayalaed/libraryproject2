var express = require('express');
var router = express.Router();
var LibEvents_dal = require('../model/LibEvents_dal');


// View All companys
router.get('/all', function(req, res) {
    LibEvents_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('LibEvents/LibEventsViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.LibDeptNum == null) {
        res.send('LibDeptNum is null');
    }
    else {
        LibEvents_dal.getById(req.query.LibDeptNum, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('LibEvents/LibEventsViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form (
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    LibEvents_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('LibEvents/LibEventsAdd', {'LibEvents': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.LibDeptNum == null)
    {
        res.send('A LibDeptNum  must be provided ');
    }
    //   else if(req.query.Fname == null)
    // {
    //   res.send('A first name must be provided');
    //}
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        LibEvents_dal.insert(req.query, function(err,result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/LibEvents/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.LibDeptNum == null) {
        res.send('A SSU id is required');
    }
    else {
        LibEmployee_dal.edit(req.query.LibDeptNum, function(err, result){
            res.render('LibEvents/LibEventsUpdate', {LibEvents: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.LibDeptNum == null) {
        res.send('A LibDeptNum id is required');
    }
    else {
        LibEvents_dal.getById(req.query.LibDeptNum, function(err, LibEvents){
            LibEvents_dal.getAll(function(err, LibEvents) {
                res.render('LibEvents/LibEventsUpdate', {LibEvents: LibEvents[0]});
            });
        });
    }

});


router.get('/update', function(req, res) {
    LibEvents_dal.update(req.query, function(err, result){
        res.redirect(302, '/LibEvents/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.LibDeptNum == null) {
        res.send('LibDeptNum is null');
    }
    else {
        LibEvents_dal.delete(req.query.LibDeptNum, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/LibEvents/all');
            }
        });
    }
});

module.exports = router;