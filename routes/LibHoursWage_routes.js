var express = require('express');
var router = express.Router();
var LibHoursWage_dal = require('../model/LibHoursWage_dal');


// View All companys
router.get('/all', function(req, res) {
    LibHoursWage_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('LibHoursWage/LibHoursWageViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.SSUID == null) {
        res.send('SSUID is null');
    }
    else {
        LibHoursWage_dal.getById(req.query.SSUID, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('LibHoursWage/LibHoursWageViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form (
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    LibHoursWage_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('LibHoursWage/LibHoursWageAdd', {'LibHoursWage': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.SSUID == null)
    {
        res.send('A SSUID  must be provided ');
    }
    //  else if(req.query.Fname == null)
    //{
    //  res.send('A first name must be provided');
    //}
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        LibHoursWage_dal.insert(req.query, function(err,result)
        {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/LibHoursWage/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.SSUID == null) {
        res.send('A SSU id is required');
    }
    else {
        LibHoursWage_dal.edit(req.query.SSUID, function(err, result){
            res.render('LibHoursWage/LibHoursWageUpdate', {LibHoursWage: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.SSUID == null) {
        res.send('A SSU id is required');
    }
    else {
        LibHoursWage_dal.getById(req.query.SSUID, function(err, LibHoursWage){
            LibHoursWage_dal.getAll(function(err, LibHoursWage) {
                res.render('LibHoursWage/LibHoursWageUpdate', {LibHoursWage: LibHoursWage[0]});
            });
        });
    }

});


router.get('/update', function(req, res) {
    LibHoursWage_dal.update(req.query, function(err, result){
        res.redirect(302, '/LibHoursWage/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.SSUID == null) {
        res.send('SSUID is null');
    }
    else {
        LibHoursWage_dal.delete(req.query.SSUID, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/LibHoursWage/all');
            }
        });
    }
});

module.exports = router;